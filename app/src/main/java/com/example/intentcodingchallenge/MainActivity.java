package com.example.intentcodingchallenge;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    // Log string
    private static final String LOG_TAG = "";

    // Intent extra for button selection.
    public static final String EXTRA_SELECT = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    // Log that the first button is clicked and then pass an intent extra to the next activity.
    public void firstPassage(View view) {
        Log.d(LOG_TAG, "Button clicked");
        Intent intent = new Intent(this, SecondActivity.class);
        intent.putExtra(EXTRA_SELECT, "button1");
        startActivity(intent);
    }

    // Log that the second button is clicked and then pass an intent extra to the next activity.
    public void secondPassage(View view) {
        Log.d(LOG_TAG, "Button clicked");
        Intent intent = new Intent(this, SecondActivity.class);
        intent.putExtra(EXTRA_SELECT, "button2");
        startActivity(intent);
    }

    // Log that the third button is clicked and then pass an intent extra to the next activity.
    public void thirdPassage(View view) {
        Log.d(LOG_TAG, "Button clicked");
        Intent intent = new Intent(this, SecondActivity.class);
        intent.putExtra(EXTRA_SELECT, "button3");
        startActivity(intent);
    }


}