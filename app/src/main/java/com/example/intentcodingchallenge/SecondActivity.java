package com.example.intentcodingchallenge;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        // Identify the textview for a passage.
        TextView textView = findViewById(R.id.passage);

        // Receive the the intent extra from the activity.
        Intent intent = getIntent();
        String selection = intent.getStringExtra(MainActivity.EXTRA_SELECT);

        // If first button was clicked, show wikipedia passage about corn.
        if(selection.equals("button1")) {
            textView.setText(R.string.cornPassage);
        }
        // If second button was clicked, show wikipedia passage about Java.
        else if(selection.equals("button2")) {
            textView.setText(R.string.javaPassage);
        }
        // If third button was clicked, show wikipedia passage about Minecraft.
        else if(selection.equals("button3")) {
            textView.setText(R.string.minecraftPassage);
        } else
            textView.setText("Could not find text.");

    }
}